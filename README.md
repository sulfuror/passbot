# passbot

_**Generate unique passwords using your old one.**_

The best password is the one you can remember. I made this script to generate multiple passwords using the same password for everything.

# Installation
```
~# wget https://gitlab.com/sulfuror/passbot/raw/master/passbot
~# chmod 755 passbot
~# mv ./passbot /usr/bin/passbot
```

# Usage
`passbot` is an interactive shell script. You just need to type `passbot` in your terminal emulator after installing it to start using it. You can see a usage example as follows:

```
~> passbot
Username: myusername
Domain/Purpose: gmail
Symbols (Y/N): y
Password: mypassword #This will not display your password; instead you will see **********
Characters (number of characters): 15
Your gmail password is: ~4~a4_FE3E522~$
```
**Optional flags**:

1. `-c, --characters`: Indicate the number of desired characters for your password.
2. `-d, --domain`: Indicate domain (i.e.: facebook, gmail, etc.)
3. `-h, --help`: Display help.
4. `-p, --password`: Indicate password.
5. `-q, --quiet`: Display result only.
5. `-s, --symbols`: Display password with symbols.
6. `-u, --username`: Indicate username.
7. `-v, --version`: Display passbot version.

Example:
```
~> passbot --username=myusername --domain=gmail --password=mypassword --characters=15 --symbols
Your gmail password is: ~4~a4_FE3E522~$
```

# How it works

`passbot` works in five steps:

1. _**Generate a passphrase**_: after indicating username, password and domain, `passbot` generate a passphrase with the three options. Using the same example above, the passphrase generated would be: `myusername-gmail-mypassword`.
2. _**Hash generated passphrase**_: once the passphrase is generated, then `passbot` proceed to hash your passphrase using `sha256`. Using this example, the hash would be `646a48fe3e5226903dbb0b406cdb10688332d9749302f88fe6960c2415658fd5`.
3. _**Generate uppercase letters**_: after the `hashed` passphrase is generated, then it will change some letters into uppercase, so you can have lower and uppercase letters. Using the same example, the modified hash would be `646a48FE3E5226903Dbb0b406cDb10688332D9749302F88FE6960c2415658FD5`.
4. _**Generate symbols**_: if you have indicated that you want symbols in your password, then `passbot` proceed to change some letters into symbols. Usint the same example, the modified hash would be `~4~a4_FE3E522~$03Dbb0b40~cDb10~_332D$@4$302F_FE~$~0c2415~5_FD5`.
5. _**Display the number characters indicated**_: finally, if you have indicated that you want a specific number of characters, then `passbot` will select the first number of letters you have indicated. Using the same example, 15 characters were selected and the final result would be `~4~a4_FE3E522~$`.

# Why?

I created `passbot` for my personal usage. I don't trust my passwords with any online service or any database. `passbot` will never save anything you type, so as long as I have a terminal emulator available (always), I can safely generate and retrieve any of my passwords.

Another reason is that I know that some sites `hash` passwords. To `hash` passwords is considered safe but sadly there are ways to decrypt hashed passwords, specially when someone uses the same for everyting and it is a very simple one. However, as long as you don't use simple passwords, you don't have to worry. That is when `passbot` comes handy because it will generate a passphrase using some data you will inmediately know and generate a unique "random" password that you can retrieve without having to save it somewhere or having to change all your passwords to something you will not remember at the end (forgot passowrd much?).